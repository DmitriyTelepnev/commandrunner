from ..Configurator import Configurator
import psycopg2


class DbConnection:

    """
    Хранитель подключений к БД
    """
    __conn = {}

    @staticmethod
    def initializeConn(serverAlias: str):
        try:
            dbConfig = Configurator.getConfig("db")
            connectionParams = dbConfig[serverAlias]
            DbConnection.__conn[serverAlias] = psycopg2.connect(
                "dbname='%s' user='%s' host='%s' password='%s' port='%s'"
                % (
                    connectionParams["database"],
                    connectionParams["user"],
                    connectionParams["host"],
                    connectionParams["password"],
                    connectionParams["port"]
                )
            )
        except:
            raise

    @staticmethod
    def getConn(serverAlias: str):
        if serverAlias not in DbConnection.__conn:
            DbConnection.initializeConn(serverAlias)
        return DbConnection.__conn[serverAlias]


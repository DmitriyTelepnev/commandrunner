from os import listdir
from os.path import isfile, join, dirname, realpath, abspath
import re
from .Includer import Includer
from .Configurator import Configurator


class TaskRunner:

    """
    Запускатель задач
    Инстанциирует задачу и логгер, инжектирует логгер в задачу, передает в задачу параметры
    Если не смог определить задачу - вернет список доступных задач
    """

    TASK_DIR_PATH = abspath(join(dirname(realpath('__file__')), './tasks'))
    TASK_NAME_REGEXP = r"Task\.py"

    @staticmethod
    def runTask(taskname: str, args: list):
        file = join(TaskRunner.TASK_DIR_PATH, taskname)
        if isfile(file):
            TaskRunner.__run(taskname, args)
            return True
        else:
            return False

    @staticmethod
    def __run(taskname, args):
        task = TaskRunner.__getTaskClass(taskname)
        logger = TaskRunner.__getLoggerClass()
        task(
            logger(taskname)
        ).run(args)

    @staticmethod
    def __getLoggerClass():
        components = Configurator.getConfig("components")
        loggernameParts = components["logger"]
        return Includer.getClass(loggernameParts["namespace"], loggernameParts["classname"])

    @staticmethod
    def __getTaskClass(taskname:str):
        tasknameParts = ("tasks/" + taskname).split("/")
        scriptName = re.sub(r"\.py$", "", tasknameParts.pop())
        tasknameParts.append(scriptName)
        return Includer.getClass(".".join(tasknameParts), scriptName)


    @staticmethod
    def getTaskList(dir=TASK_DIR_PATH, preffix=None):
        tasks = []
        for directoryItem in listdir(dir):
            pathItem = join(dir, directoryItem)
            if isfile(pathItem):
                if re.search(TaskRunner.TASK_NAME_REGEXP, pathItem):
                    tasks.append((preffix and preffix + "/" or "") + directoryItem)
            else:
                tasks.extend(TaskRunner.getTaskList(pathItem, directoryItem))

        return tasks

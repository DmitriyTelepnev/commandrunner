class EtlTask:
    """
    Класс задачи, у которого есть метод run и logger
    """
    logger = None

    def __init__(self, logger=None):
        self.logger = logger

    def run(self, args):
        pass

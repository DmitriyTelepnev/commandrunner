from engine.Logger import Logger


class CustomLogger(Logger):

    __loggedClass = ""

    _template = "----- %s ----- %s"

    DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

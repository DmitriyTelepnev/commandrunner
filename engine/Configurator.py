import json, os
from engine.BadConfigException import BadConfigException


class Configurator:
    """
    Класс, держащий в памяти конфигурацию приложения
    """

    _cfg = {}

    @staticmethod
    def getConfig(key):
        if key in Configurator._cfg.keys():
            return Configurator._cfg[key]
        else:
            raise BadConfigException("Нет такого ключа конфигурации")


currentDirectory = os.path.dirname(__file__)

with open(currentDirectory + "/configs/application.json", 'r') as jsonFile:
    Configurator._cfg = json.load(jsonFile)

from engine.EtlTask import EtlTask
from engine.db.DbConnection import DbConnection
from psycopg2.extras import RealDictCursor


class UserTask(EtlTask):
    def run(self, args):
        self.writeUsers(
            self.getUsers()
        )

    def getUsers(self):
        connection = DbConnection.getConn("staging")
        cursor = connection.cursor(cursor_factory=RealDictCursor)
        cursor.execute("""
                    SELECT * FROM opentech.users
                """)
        users = cursor.fetchall()

        self.logger.info("Выбрал %s пользователей" % (len(users)))

        return users

    def writeUsers(self, users):
        connection = DbConnection.getConn("staging")
        cursor = connection.cursor()

        query = "INSERT INTO _trash.users VALUES "
        values = []
        for user in users:
            values.append("(%s, '%s', '%s')" % (user["id"], user["name"], user["login"]))
        query = query + ",".join(values)
        cursor.execute(query)
        connection.commit()
        self.logger.info("записал %s пользователей" % (len(users)))

class Includer:
    """
    Автолоадер
    """
    @staticmethod
    def getClass(namespace: str, classname: str):
        mod = __import__(namespace, fromlist=[classname])
        return getattr(mod, classname)

<h2>Проект для выполнения каких-то консольных комманд</h2>

Для простейшего исполь зования необходимо:
1) написать Task, отнаследованный от EtlTask
2) Выполнить в консоли `./run.py test/UserTask.py`

Для использования с параметрами
1) см. выше
2) Выполнить в консоли `./run.py test/UserTask.py param1 param2 param3`

Для использования логгера в Task'е использовать `self.logger`
Для переопределения логгера исправить в engine/configs/application.json

```json
  ...
  "components": {
    "logger": {
      "namespace": "components.CustomLogger",
      "classname": "CustomLogger"
    }
  }
  ...
```

Для работы с БД использовать `DbConnection.getConn(serverAlias)`,
передавая в serverAlias - алиас параметров подключения к бд, прописанных в engine/configs/application.json 

PS: таски важно распологать в папке tasks, в любой поддиректории

Если вызвать `./run.py` без определения задачи, или с задачей, которой нет - 
вам выведутся задачи, доступные для исполнения
```text
Укажите задачу, которую нужно запустить
- test/UserTask.py
```
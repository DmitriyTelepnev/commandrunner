#!/usr/bin/python3
from sys import argv
from engine.TaskRunner import TaskRunner
from engine.TaskListPrinter import TaskListPrinter

if len(argv) > 1:
    argv.pop(0)
    taskname = argv.pop(0)

    success = TaskRunner.runTask(taskname, argv)

    if success:
        exit(0)

TaskListPrinter.print(
    TaskRunner.getTaskList()
)

from datetime import datetime


class Logger:

    """
    Логгер
    """
    __loggedClass = ""

    _template = "[%s]\t%s"

    DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

    def __init__(self, loggedClass):
        self.__loggedClass = loggedClass

    def info(self, message):
        date = datetime.now()
        print(
            date.strftime(self.DATETIME_FORMAT),
            self._template % (self.__loggedClass, message),
        )
